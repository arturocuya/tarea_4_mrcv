// Ejercicio 3: Dado el array "inventario", definir una función que tome como parámetro 
// una cantidad de unidades y devuelva otro array de estructura similar que solo
// contenga los productos con stock mayor o igual al número

// Revisar: https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/filter

// NO utilizar for loops
// Utilizar la función filter()

const inventario = [
  {
    nombre: 'Gaseosa',
    stock: 100
  },
  {
    nombre: 'Pan',
    stock: 20
  },
  {
    nombre: 'Chocolate',
    stock: 30
  },
  {
    nombre: 'Baterías',
    stock: 10
  },
  {
    nombre: 'Leche',
    stock: 50
  },
  {
    nombre: 'Lapiceros',
    stock: 400
  },
  {
    nombre: 'Helado',
    stock: 15
  },
]

// const mayorACincuenta = foo(inventario, 50)
// console.log(mayorACincuenta) // Incluiría gaseosa, leche y lapiceros
