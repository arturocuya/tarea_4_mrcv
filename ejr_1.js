// Ejercicio 1: Asignación deestructurante
// Revisar: https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Operadores/Destructuring_assignment
// Revisar: https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/template_strings
// Revisar: https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/join

// A partir de la variable data, imprimir en consola
// "Hay 4 miembros en la familia Cuya Reyes. Estos son: Arturo, Manuel, Jessica, Ted"

// Utilizar asignación deestructurante, interpolación de expresiones y la función join()
// NO utilizar bucles for ni acceso al arreglo por índice (ej: data[0])

const data = [
  'Cuya Reyes',
  [
    'Arturo',
    'Manuel',
    'Jessica',
    'Ted'
  ]
]
