// Ejercicio 2: Crear una función que reciba como parámetro un arreglo de números y retornar en la consola los elementos elevados al cuadrado solo si se cumple que el número es positivo.

// Revisar: https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/map
// Revisar: https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Funciones/Arrow_functions

// Utilizar la función map() para recorrer el arreglo de números
// Utilizar sintaxis de flecha para definir la función

const numbers = [2,3,7,-1,-2,0,5,-7]

// foo(numbers)
