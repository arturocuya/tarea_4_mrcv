// Ejercicio 4: Definir una función que determine si la cadena de texto que se le pasa como parámetro es un palíndromo,
// es decir, si se lee de la misma forma desde la izquierda y desde la derecha.
// Ejemplo de palíndromo complejo: "La ruta nos aporto otro paso natural".

// Resolver el ejercicio usando un solo for loop
// Definir la función utilizando sintaxis de flecha
// No considerar tildes ni mayúsculas

const texto = "la ruta nos aporto otro paso natural"

// Dentro de la función definida:
// Paso 1: Limpiar espacios en blanco utilizando la función replaceAll()
// Paso 2: Determinar si es palíndromo
